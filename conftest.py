import app
import pytest
from requests import get
import os

target=os.getenv('TEST_ADDR','http://10.0.0.2:5000')

@pytest.fixture
def app():
    app = app.create_app()
    return app

def test_myapp():
    assert get(target+"?name=4").status_code == 200

#def test_failure():
#    assert get(target+"?name=BLOWUP").status_code == 200
